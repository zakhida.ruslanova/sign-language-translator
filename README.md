# Link to Vercel-app: 

https://sign-language-translator-l3z9zuqea-zakhidaruslanova.vercel.app/

# Following tools are us

- Figma
- NPM/Node.js (LTS – Long Term Support version)
- React CRA (create-react-app)
- Visual Studio Code Text Editor/ IntelliJ
- Browser Developer Tools for testing and debugging
- React and Dev Tools
- Git
- Rest API- Heroku

# How to run the application:

- npm start

# Startup/Login Page

The first thing a user will see is the “Login page” where the user is able to enter his name. 
The username is saved to the Translation API. The app first checks if the user exists. Once the name has been stored in 
the API, the app displays the main page, the translation page. 
Users that are already logged in are automatically redirected to the Translation page. 

# Translation Page

A user views only this page if they are currently logged into the app. The user types in the input box at the top of the page. The user must click on the “translate” button to the right of the input box to trigger the translation. 
Translations is be stored using the API. The Sign language characters appear in the “translated” box, under "Result". The limit of the input is 40 letters. Only the letters may be typed. Only the text is be stored, not the sign language images.

# Profile page

The profile page displays the last 10 translations for the current user. Only the text of the 
translation is despalyed. There is a button to clear the translations. This “deletes” in my API and no longer display on the profile page. To simplify things, you may simply delete the records, but you should NEVER delete data from a database.
The Logout button clears all the storage and returns to the start page.

# Features

- React framework
- React Router to navigate between pages
- API to store the users and their translations
- the ContextAPI is used to manage state