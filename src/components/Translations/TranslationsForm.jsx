import { useForm } from "react-hook-form"

const TranslationsForm = ({ onTranslation }) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = ({ translationNotes }) => {
        onTranslation(translationNotes)
    }
    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
                <div className="mt-3 block w-full px-3 py-2 bg-white border border-slate-300 rounded-md text-sm shadow-sm placeholder-slate-400
      focus:outline-none flex justify-between">
                    <input htmlFor="translation-notes" type="text" {...register('translationNotes')} placeholder="text to translate" className="w-full ml-3 bg-white" />
                    <span><button type="submit" className="  w-fit px-3 py-2 ml-10  border-slate-300 rounded-md text-m shadow-sm bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 ...">
                        Translate</button></span>
                </div>
            </fieldset>
        </form>
    )
}
export default TranslationsForm