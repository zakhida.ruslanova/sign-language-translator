const TranslatedImages = ({ images }) => {
    return (
            <div className="container p-0 my-5 text-white flex items-stretch">
                {images.map((img,index) => {return <div><img key={index} src={img} width="60"/></div>})}
            </div>
    )
}

export default TranslatedImages