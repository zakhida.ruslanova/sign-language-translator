import { useUser } from "../../context/UserContext"

const Navbar = () => {

    const { user } = useUser()
    return (
        <li class="nav-item dropdown bg-amber-300 p-3 justify-content-center sticky-top flex items-stretch">

            <div class="container-fluid  flex items-stretch">
                <a class="navbar-brand font-bold text-gray-500" href="#">Lost in translation</a>
            </div>
            {user !== null &&
                <div>
                    <a class="nav-link dropdown-toggle font-bold text-gray-500 snap-center" href="#" role="button" data-bs-toggle="dropdown">Menu</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="/profile">Profile</a></li>
                        <li><a class="dropdown-item" href="/translations">Translations</a></li>
                    </ul>
                </div>
            }
        </li>
    )
}
export default Navbar