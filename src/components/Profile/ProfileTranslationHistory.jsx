import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({ translations }) => {

    const translationList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={index + '-' + translation} translation={translation} />)
    return (
        <section>
            <div className="container rounded bg-white mt-1 mb-3">
                <div class="col-md-5 border-right block">
                    <div class="p-3">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right font-bold">YourTranslations history</h4>
                        </div>
                        <div class="d-flex justify-content-between align-items-center ">
                            <h4 className="text-right">{translationList.length === 0 && <p className="text-right">You have no translations yet</p>}</h4>
                        </div>
                    </div>
                </div>
            </div>

            <ul className="list-group">
                {translationList.slice(0).slice(-10)}
            </ul>
        </section>
    )
}

export default ProfileTranslationHistory