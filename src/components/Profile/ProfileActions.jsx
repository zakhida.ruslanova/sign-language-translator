import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { storageDelete, storageSave } from '../../utils/storage'
import { useUser } from '../../context/UserContext'
import { translationClearHistory } from '../../api/translation'

const ProfileActions = () => {

    const { user, setUser } = useUser()

    const handleLogoutClick = ({ }) => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearHistoryClick = async () => {
        if (!window.confirm('Are you sure?')) {
            return
        }

        const [clearError] = await translationClearHistory(user.id)


        if (clearError !== null) {
            return
        }

        const updateUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER,updateUser)
        setUser(updateUser)
    }
    return (
        <ul className="mt-5 text-center">
            <li><button className=" w-fit px-3 py-2 my-2 border-slate-300 rounded-md text-m shadow-sm bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 ..." onClick={handleClearHistoryClick}>Clear history</button> </li>
            <li><button className=" w-fit px-3 py-2 my-2 border-slate-300 rounded-md text-m shadow-sm bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 ..." onClick={handleLogoutClick}>Logout</button></li>
        </ul>
    )
}

export default ProfileActions