const ProfileHeader = ({ username }) => {
    return (
        <div class="container rounded bg-white my-1">
            <div class="row">
                <div class="col-md-12t border-righ">
                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                        <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg" />
                        <span class="font-weight-bold">{username}</span><span class="text-black-50">{username}@.mail.com</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default ProfileHeader