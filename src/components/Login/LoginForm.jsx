import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {
    required: true,
    minLength: 3,
}

const LoginForm = () => {
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    useEffect(() => {
        if (user !== null) { navigate('/translations') }

    }, [user, navigate])

    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }

    console.log(errors)

    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }
        if (errors.username.type === 'minLength') {
            return <span> Username is too short </span>
        }
    })()

    return (

        <div class="container-fluid">
            <div class="row">
                <div className="bg-amber-300  grid grid-rows-4">
                    <div className=" my-2 align-items-center">
                        <div className="">
                            <h1 className="text-4xl animate-pulse w-10 h-10 font-serif ml-3">Lost in translation</h1>
                        </div>
                        <img src="robot.png" className="mt-10 "></img>
                    </div>
                    <form onSubmit={handleSubmit(onSubmit)} >
                        <label className="block">
                            <input type="text" placeholder="What's your name" className="mt-1 block w-1/2 px-3 py-2 bg-white border border-slate-300 rounded-md text-sm shadow-sm placeholder-slate-400
      focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500
      disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none
      invalid:border-pink-500 invalid:text-pink-600
      focus:invalid:border-pink-500 focus:invalid:ring-pink-500
    " {...register("username", usernameConfig)} />
                            {errorMessage}
                        </label>
                        <button type="Submit" disabled={loading} className="mt-3 block w-min px-3 py-2 border-slate-300 rounded-md text-m shadow-sm bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 ...">
                            Continue
                        </button>
                        {loading && <p>Logging in...</p>}
                        {apiError && <p>{apiError}</p>}
                    </form>
                </div>
            </div>
        </div>

    )
}
export default LoginForm