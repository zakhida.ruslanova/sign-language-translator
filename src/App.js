import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Login from './views/Login';
import Profile from './views/Profile';
import Translations from './views/Translations';
import Navbar from './components/Navbar/Navbar';

function App() {
  return (
    
    <BrowserRouter>
    <div className="App">
    <Navbar/>
      <Routes>
        <Route path="/" element={ <Login /> } />
        <Route path="/translations" element={ <Translations />} />
        <Route path="/profile" element={ <Profile /> } />
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
