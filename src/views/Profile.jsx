import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import withAuth from "../hoc/withAuth"
import { useUser } from "../context/UserContext"
import { useEffect } from "react"
import { userById } from "../api/user"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"

const Profile = () => {
    const { user, setUser } = useUser()

    useEffect(() => {
        const findUser = async () => {
            const [error, lattestUser] = await userById(user.id)
            if (error === null) {
                storageSave(STORAGE_KEY_USER, lattestUser)
                setUser(lattestUser)
            }
        }
    }, [setUser, user.id])

    return (
        <>
            <ProfileHeader username={user.username} />
            <ProfileTranslationHistory translations={user.translations} />
            <ProfileActions />

        </>
    )
}

export default withAuth(Profile) 