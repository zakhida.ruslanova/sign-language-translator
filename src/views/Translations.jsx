import { useState } from "react"
import { translationAdd } from "../api/translation"
import TranslationsForm from "../components/Translations/TranslationsForm"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import TranslatedImages from "../components/Translations/TranslatedImages"

const Translations = () => {

    const [textToBeTranslated, setTextToBeTranslated] = useState(null)
    const [mynewarray, setMyNewArray] = useState([])
    const { user, setUser } = useUser()

    const handleTranslationClicked = async (textToBeTranslated) => {


        if (!textToBeTranslated) {
            alert("Please write something at first")
            return
        }

        if (textToBeTranslated.length > 40) {
            alert("Limit of input is 40 letters... You typed: " + textToBeTranslated.length)
            return
        }

        if (!/^[a-zA-Z]+$/.test(textToBeTranslated)) {
            alert("Type only letters... You typed: " + textToBeTranslated)
            return
        }

        const translation = (textToBeTranslated)
        const [error, updatedUser] = await translationAdd(user, translation)

        if (error !== null) {
            return
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)

        const urlImg = "individial_signs/"
        const mynewarray = Array.from(textToBeTranslated.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')).map(letter =>
            urlImg + letter.toLowerCase() + ".png")

        setMyNewArray(mynewarray)
    }

    return (
        <>
            <section id="translation-notes">
                <TranslationsForm onTranslation={handleTranslationClicked} />
            </section>
            <h4 className="text-left font-bold mt-4 mx-4"> Result </h4>
            <section id="translated-images" >
                <div className="flex items-stretch w-50">
                    <TranslatedImages images={mynewarray} />
                </div>
            </section>
        </>
    )
}
export default withAuth(Translations)